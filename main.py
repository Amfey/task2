import functions as fun

def blit(number):
    print number
    stores = fun.check_input_and_return_number_stores_of_piramid(number)
    if stores == False:
        return "You can not do that with this number"
    piramid = fun.create_list(number, stores)
    tri_stores = fun.convert_to_stores(piramid, stores)
    last_store_tri = fun.get_num_tri_last_store(tri_stores)
    triangles = fun.split_to_triangles(piramid)
    changed_triangles = fun.change_triangles(triangles)


    reduced_tri = fun.reduce(changed_triangles)
    to_reduce = ""
    output_not_reduced = fun.merge_triangles_store(changed_triangles, last_store_tri)
    print output_not_reduced
    if reduced_tri == "Can not reduce":
        return output_not_reduced

    while reduced_tri != "Can not reduce":

        print reduced_tri

        reduced_stores = fun.check_input_and_return_number_stores_of_piramid(reduced_tri)
        piramid = fun.create_list(reduced_tri, reduced_stores)
        tri_stores = fun.convert_to_stores(piramid, stores)
        last_store_tri = fun.get_num_tri_last_store(tri_stores)
        triangles = fun.split_to_triangles(piramid)
        changed_triangles = fun.change_triangles(triangles)

        print fun.merge_triangles_store(changed_triangles, last_store_tri)

        to_reduce = fun.merge_triangles_store(changed_triangles, last_store_tri)
        reduced_tri = fun.reduce(to_reduce)
    if not to_reduce == "":
        return to_reduce