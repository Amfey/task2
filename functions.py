
# return strores if number is valid
def check_input_and_return_number_stores_of_piramid(number):
    i = len(number)
    a = i
    j = 0
    while a > 1:
        a = a / 4.0
        j = j + 1
    if a == 1:
        return 2**j
    else:
        return False

def create_list(number, stores):
    piramid = []
    list_length_stores = [ 1 + 2 * a for a in range(stores)]
    for slice_length in reversed(list_length_stores):
        slice = number[:slice_length][::-1]
        piramid.append(slice)
        number = number[slice_length:]
    return piramid


def get_num_tri_last_store(store):
    return ((len(store[0]) / 4) * 2) + 1

def create_triangles(store):
    tri_list = []
    triangle = ""
    num_tri_store = get_num_tri_last_store(store)
    while store[0] != '' and store[1] != 0:
        for i in range(num_tri_store):
            if i % 2 == 0:
                triangle = store[0][-3:][::-1]
                store[0] = store[0][:-3]
                triangle += store[1][-1:]
                store[1] = store[1][:-1]
                tri_list.append(triangle)
            else:
                triangle = store[1][-3:][::-1]
                store[1] = store[1][:-3]
                triangle += store[0][-1:]
                store[0] = store[0][:-1]
                tri_list.append(triangle)
    return tri_list


def split_to_triangles(piramid):
    num_stores_triangles = len(piramid) / 2
    triangles = []
    for a in range(num_stores_triangles):
        tri_store = piramid[a*2:(a+1)*2]
        triangles.append(create_triangles(tri_store))
    if any(isinstance(i, list) for i in triangles):
        return sum(triangles, [])
        return triangles
    else:
        return triangles

change_patterns = {"0000": "0000",
                   "0001": "1000",
                   "0010": "0001",
                   "0011": "0010",
                   "0100": "0000",
                   "0101": "0010",
                   "0110": "1011",
                   "0111": "1011",
                   "1000": "0100",
                   "1001": "0101",
                   "1010": "0111",
                   "1011": "1111",
                   "1100": "1101",
                   "1101": "1110",
                   "1110": "0111",
                   "1111": "1111",
                   }

def change_triangles(triangles):
    changed_triangles = []
    for triangle in triangles:
        if triangle in change_patterns.keys():
            triangle = change_patterns[triangle]
        else:
            triangle = triangle
        changed_triangles.append(triangle)
    return changed_triangles


def convert_to_stores(triangles, num_tri_store):
    stores_tri = []
    while triangles != []:
        stores_tri.append(triangles[:num_tri_store])
        triangles = triangles[num_tri_store:]
        num_tri_store -= 2
    return  stores_tri

# test_stores = ['0000', '1111', '1111', '1111']
# test_storesfalse = ['0000', '1111', '1111', '1110']
# test_stores2 = ['0000', '0000', '0000', '1111', '0000', '0000', '0000', '0000', '1111', '1111', '1111', '0000', '0000', '0000', '0000', '0000']

def reduce(triangles):
    red_triangles = []
    for triangle in triangles:
        if triangle == "0000":
            red_triangles.append("0")
        elif triangle == "1111":
            red_triangles.append("1")
        else:
            red_triangles.append("False")
            break
    if "False" in red_triangles:
        return "Can not reduce"
    else:
        return "".join(red_triangles)

def last_store_to_num(store, last_store_tri):
    out_string = ""
    two_lines = [[],[]]
    while store !=[]:
        for i in range(last_store_tri):
            if i % 2 == 0:
                out_string += store[i][:3]
                store[i] = store[i][3:]
            else:
                out_string += store[i][:1]
                store[i] = store[i][1:]
        second_line = "".join(store)
        out_string = out_string + second_line
        store = []
        return out_string


def merge_triangles_store(triangles, last_store_tri):
    output = ""
    new_triangles = triangles
    while new_triangles != []:
        last_store = new_triangles[:last_store_tri]
        new_triangles = new_triangles[last_store_tri:]
        last_store_tri -= 2
        output += last_store_to_num(last_store, last_store_tri)
        new_triangles = new_triangles[:last_store_tri]
    return output