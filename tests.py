import unittest
from functions import check_input_and_return_number_stores_of_piramid
from functions import create_list, change_triangles, reduce
from main import blit

class MyTestsCheckSum(unittest.TestCase):

    def test_check_correct_length(self):
        input = "1"*4
        self.assertEqual(check_input_and_return_number_stores_of_piramid(input), 2)

    def test_check_correct_long_length(self):
        input = "1"*256
        self.assertEqual(check_input_and_return_number_stores_of_piramid(input), 16)

    def test_check_incorrect_long_length(self):
        input = "1"*260
        self.assertEqual(check_input_and_return_number_stores_of_piramid(input), False)

    def test_create_list(self):
        test_number1 = "0101110110100110"
        test_number2 = "0000011100001000"
        test_stores = 4
        inp1 = create_list(test_number1,test_stores)
        inp2 = create_list(test_number2,test_stores)
        out1 =['0111010', '01011', '110', '0']
        out2 = ['1100000', '00001', '001', '0']
        self.assertEqual(inp1, out1)
        self.assertEqual(inp2, out2)

    def test_change_triangles(self):
        triangles = ['0101', '1011', '1100', '0110']
        inp = change_triangles(triangles)
        out = ['0010', '1111', '1101', '1011']
        self.assertEqual(inp, out)

    def test_reduce(self):
        test_stores = ['0000', '1111', '1111', '1111']
        test_storesfalse = ['0000', '1111', '1111', '1110']
        test_stores2 = ['0000', '0000', '0000', '1111', '0000', '0000', '0000', '0000', '1111', '1111', '1111', '0000', '0000', '0000', '0000', '0000']
        inp1 = reduce(test_stores)
        inp2 = reduce(test_storesfalse)
        inp3 = reduce(test_stores2)
        out1 = '0111'
        out2 = 'Can not reduce'
        out3 = '0001000011100000'
        self.assertEqual(inp1, out1)
        self.assertEqual(inp2, out2)
        self.assertEqual(inp3, out3)


class MyTestsBlit(unittest.TestCase):

    def test_given_number(self):
        input = "0011110011111011"
        self.assertEqual((blit(input)), "0001111111101111")

    def test_can_reduce(self):
        input = "0001000011101111"
        self.assertEqual(blit(input), '0010')

    def test_mybirth(self):
        input = "001111011110110"
        self.assertEqual(blit(input), "You can not do that with this number")